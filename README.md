# Тестовое задание

## Запуск

### Вариант 1

```shell
$ cd src/
$ docker-compose build
$ docker-compose up
```

### Вариант 2

```shell
$ docker-compose pull
$ docker-compose up
```

## Запросы

Запросы отправляются на `locahost:8888`

### POST запрос для отправки данных

```json
{
    "array": [5,3,1,5,7,4,3]
}
```

Если массив обработался, то сервер вернёт JSON:
```json
{
    "result": "OK",
    "id": 1
}
```

иначе:

```text
{
"ERROR"
}
```

### GET запрос для получения данных

```json
{
    "id": 1
}
```

Если массив обработался, то сервер вернёт JSON:
```json
{
    "id": 1,
    "array": [5,3,1,5,7,4,3],
    "result_array": [1,3,3,4,5,5,7],
    'date_of_creation': datetime.datetime(2021,4, 8, 11, 41, 32, 707509, tzinfo=psycopg2.tz.FixedOffsetTimezone(offset=0, name=None))
}
```

иначе:

```text
{
"ERROR"
}
```
