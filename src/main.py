from abc import ABC

import tornado.ioloop
import tornado.web

from sqlalchemy.exc import SQLAlchemyError

from datetime import datetime

from dbclass import DataBase, Array
from migrate import run_migration_upgrade
from test_connect import test_connection

import CONFIG


def check_array(data):
    for i in data:
        if type(i) != int:
            raise ValueError


async def data_processing(data):
    check_array(data['array'])
    date_of_creation = datetime.now()
    result_array = data['array'].copy()
    result_array.sort()
    db_array = Array(data['array'], result_array, date_of_creation)
    array_id = db.input_data(db_array)
    return array_id


class RootHandler(tornado.web.RequestHandler, ABC):
    async def get(self):
        try:
            data = self.get_arguments('id')
            array = db.get_data_by_id(int(data[0]))
            self.write(str(array))
        except SQLAlchemyError:
            self.write("ERROR")
        except AttributeError:
            self.write("ERROR")
        except Exception:
            self.write("ERROR")

    async def post(self):
        try:
            data = tornado.escape.json_decode(self.request.body)
            array_id = await data_processing(data)
            self.write({"result": "OK", "id": array_id})
        except ValueError:
            self.write("ERROR")
        except SQLAlchemyError:
            self.write("ERROR")

    def set_default_headers(self, *args, **kwargs):
        self.set_header("Content-Type", "application/json")


def make_app():
    return tornado.web.Application([
        (r"/", RootHandler),
    ])


if __name__ == "__main__":
    test_connection()
    run_migration_upgrade()
    db = DataBase(CONFIG.DB_USER, CONFIG.DB_PASSWORD, CONFIG.DB_NAME, host=CONFIG.DB_HOST, port=CONFIG.DB_PORT,
                  docker=True)
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
