from sqlalchemy import create_engine
import time
from sqlalchemy.exc import OperationalError


def test_connection():
    while True: 
        try:
            db_connection = create_engine("postgresql+psycopg2://tz_arrays:polosh@db/tz_arrays")
            db_connection.connect()
        except OperationalError:
            print("Not connected")
            time.sleep(5)
        else:
            print("Connected")
            break
        

if __name__ == "__main__":
    test_connection()
