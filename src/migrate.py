from alembic import command
from alembic.config import Config

def run_migration_upgrade():
    alembic_cfg = Config("alembic.ini")
    # alembic_cfg.set_main_option("script_location", "./alembic/versions")
    # alembic_cfg.set_main_option("sqlalchemy.url", "postgresql+psycopg2://tz_arrays:polosh@db/tz_arrays")
    command.upgrade(alembic_cfg, "head")