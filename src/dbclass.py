from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, MetaData, ARRAY, TIMESTAMP
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


Base = declarative_base()


class Array(Base):
    metadata = MetaData()
    __tablename__ = "arrays"
    id = Column(Integer, primary_key=True)
    array = Column(ARRAY(Integer))
    result_array = Column(ARRAY(Integer))
    date_of_creation = Column(TIMESTAMP(timezone=True))

    def __init__(self, array, result_array, date_of_creation):
        self.array = array
        self.result_array = result_array
        self.date_of_creation = date_of_creation

    def __repr__(self):
        return "<Array('{}','{}', '{}')>".format(self.array, self.result_array, self.date_of_creation)


class DataBase:
    def __init__(self, login, password, db_name, host='localhost', port='5432', docker=True):
        if docker:
            host = "db"
        else:
            host = "{}:{}".format(host, port)
        self.db_connection = create_engine("postgresql+psycopg2://{}:{}@{}/{}".format(login, password, host, db_name))
        self.Sessions_fabric = sessionmaker(bind=self.db_connection)
        self.session = self.Sessions_fabric()

    def input_data(self, data):
        self.session.add(data)
        self.session.commit()
        return data.id

    def get_data_by_id(self, aid):
        array = self.session.query(Array).filter_by(id=aid).first()
        return {
            "id": array.id,
            "array": array.array,
            "result_array": array.result_array,
            "date_of_creation": array.date_of_creation,
        }
